package log

import "go.uber.org/zap"

var Logger *zap.Logger = zap.NewExample()

func SetGlobalLogger(logger *zap.Logger) {
	Logger = logger
}
