package recorder

import (
	"fmt"
	"github.com/skypies/adsb"
)

var registeredRecorders = make(map[string]map[uint]Recorder)

type Recorder interface {
	Configure(configMap map[string]interface{}) error
	Open() error
	Close() error
	RecordMessage(message *adsb.Msg) error
}

func registerRecorder(typeName string, version uint, recorder Recorder) error {
	if _, ok := registeredRecorders[typeName]; !ok {
		registeredRecorders[typeName] = make(map[uint]Recorder)
	}
	if _, ok := registeredRecorders[typeName][version]; ok {
		return fmt.Errorf("recorder %s version %d already registered", typeName, version)
	}
	registeredRecorders[typeName][version] = recorder
	return nil
}

// GetRecorder returns the recorder for the given type and version
func GetRecorder(typeName string, version uint) (Recorder, error) {
	if rec, ok := registeredRecorders[typeName][version]; ok {
		return rec, nil
	} else {
		return nil, fmt.Errorf("recorder %s version %d not registered", typeName, version)
	}
}

// GetConfiguredRecorder returns the recorder for the given type and version and configures it with the given config map
func GetConfiguredRecorder(typeName string, version uint, configMap map[string]interface{}) (Recorder, error) {
	rec, err := GetRecorder(typeName, version)
	if err != nil {
		return nil, err
	}
	if err := rec.Configure(configMap); err != nil {
		return nil, err
	}
	return rec, nil
}
