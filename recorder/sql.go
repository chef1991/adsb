package recorder

import (
	"fmt"
	"github.com/skypies/adsb"
	"gitlab.com/chef1991/adsb/internal/log"
	"strings"
	"time"

	"gorm.io/driver/clickhouse"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

type SqlRecorder struct {
	config SqlRecorderConfig
	db     *gorm.DB
}

var validSqlDrivers = []string{
	"mysql",
	"postgres",
	"sqlite3",
	"clickhouse",
	"sqlserver",
}

type SqlRecorderConfig struct {
	driver      string
	dsn         string
	autoMigrate bool
}

func (r *SqlRecorder) openMySql() gorm.Dialector {
	return mysql.Open(r.config.dsn)
}

func (r *SqlRecorder) openPostgres() gorm.Dialector {
	return postgres.Open(r.config.dsn)

}

func (r *SqlRecorder) openSqlite3() gorm.Dialector {
	return sqlite.Open(r.config.dsn)
}

func (r *SqlRecorder) openSqlServer() gorm.Dialector {
	return sqlserver.Open(r.config.dsn)
}

func (r *SqlRecorder) openClickhouse() gorm.Dialector {
	return clickhouse.Open(r.config.dsn)
}

func (s *SqlRecorder) Open() error {
	logger := log.Logger
	sLogger := logger.Sugar()
	logger.Debug("opening sql recorder database connection")
	var dialector gorm.Dialector
	switch s.config.driver {
	case "mysql":
		dialector = s.openMySql()
	case "postgres":
		dialector = s.openPostgres()
	case "sqlite3":
		dialector = s.openSqlite3()
	case "clickhouse":
		dialector = s.openClickhouse()
	case "sqlserver":
		dialector = s.openSqlServer()
	default:
		return fmt.Errorf("unknown sql driver: %s", s.config.driver)
	}
	logger.Debug("sql recorder database connection opened")
	if tempDb, err := gorm.Open(dialector); err != nil {
		return fmt.Errorf("error opening sql recorder: %w", err)
	} else {
		s.db = tempDb
	}
	if s.config.autoMigrate {
		sLogger.Debugw("auto-migrating sql recorder database")
		err := s.db.AutoMigrate(&MessageModel{})
		if err != nil {
			sLogger.Errorw("error auto-migrating sql recorder database", "error", err)
			return fmt.Errorf("error auto-migrating sql recorder: %w", err)
		}
		logger.Debug("auto-migrated sql recorder database")
	}
	return nil
}

func (s *SqlRecorder) Close() error {
	sqlDb, err := s.db.DB()
	if err != nil {
		return err
	}
	return sqlDb.Close()
}

func (s *SqlRecorder) RecordMessage(message *adsb.Msg) error {
	model := MessageModel{
		Type:                  message.Type,
		SubType:               message.SubType,
		IcaoHex:               string(message.Icao24),
		GeneratedTimestampUtc: &message.GeneratedTimestampUTC,
		LoggedTimestampUtc:    &message.LoggedTimestampUTC,
		Callsign:              message.Callsign,
		Altitude:              message.Altitude,
		GroundSpeed:           message.GroundSpeed,
		Track:                 message.Track,
		VerticalRate:          message.VerticalRate,
		Squawk:                message.Squawk,
		AlertSquawkChange:     message.AlertSquawkChange,
		Emergency:             message.Emergency,
		Spi:                   message.SPI,
		IsOnGround:            message.IsOnGround,
		NumStations:           message.NumStations,
	}
	if err := s.db.Create(&model).Error; err != nil {
		return fmt.Errorf("error recording message: %w", err)
	}
	return nil
}

type MessageModel struct {
	ID                    uint       `gorm:"primary_key;column:pid"`
	Type                  string     `gorm:"column:type"`
	SubType               int64      `gorm:"column:subType"`
	IcaoHex               string     `gorm:"column:icaoHex"`
	GeneratedTimestampUtc *time.Time `gorm:"column:generatedTimestampUtc"`
	LoggedTimestampUtc    *time.Time `gorm:"column:loggedTimestampUtc"`
	Callsign              string     `gorm:"column:callsign"`
	Altitude              int64      `gorm:"column:altitude"`
	GroundSpeed           int64      `gorm:"column:groundSpeed"`
	Track                 int64      `gorm:"column:track"`
	VerticalRate          int64      `gorm:"column:verticalRate"`
	Squawk                string     `gorm:"column:squawk"`
	AlertSquawkChange     bool       `gorm:"column:alertSquawkChange"`
	Emergency             bool       `gorm:"column:emergency"`
	Spi                   bool       `gorm:"column:spi"`
	IsOnGround            bool       `gorm:"column:isOnGround"`
	NumStations           int64      `gorm:"column:numStations"`
}

func (MessageModel) TableName() string {
	return "messages"
}

func (s *SqlRecorder) Configure(configMap map[string]interface{}) error {
	logger := log.Logger
	sLogger := logger.Sugar()
	sLogger.Infow("configuring sql recorder", "config", configMap)
	if IrawDriver, ok := configMap["driver"]; !ok || IrawDriver == nil {
		return fmt.Errorf("error: missing 'driver' config for sql recorder")
	} else {
		driverStr := IrawDriver.(string)
		driverStr = strings.ToLower(driverStr)
		validDriver := false
		for _, d := range validSqlDrivers {
			if driverStr == d {
				validDriver = true
				break
			}
		}
		if !validDriver {
			return fmt.Errorf("error: invalid 'driver' config for sql recorder: %s", driverStr)
		}
		sLogger.Debugw("setting driver", "driver", driverStr)
		s.config.driver = driverStr
	}
	if Idsn, ok := configMap["dsn"]; !ok || Idsn == nil {
		return fmt.Errorf("error: missing 'dsn' config for sql recorder")
	} else {
		s.config.dsn = Idsn.(string)
	}
	if IautoMigrate, ok := configMap["automigrate"]; !ok || IautoMigrate == nil {
		logger.Warn("no 'autoMigrate' config for sql recorder, defaulting to true")
		s.config.autoMigrate = false
	} else {
		logger.Debug("setting autoMigrate config for sql recorder")
		s.config.autoMigrate = IautoMigrate.(bool)
	}
	sLogger.Debugw("configured sql recorder", "config", s.config)
	fmt.Printf("config: %+v\n", s.config)
	return nil
}

func init() {
	if err := registerRecorder("sql", 1, &SqlRecorder{}); err != nil {
		panic(fmt.Errorf("error registering sql recorder: %w", err))
	}
}
