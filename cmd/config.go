package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/chef1991/adsb/recorder"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"strings"
)

type LogLevel uint8

const (
	LegLevelDebug LogLevel = iota
	LogLevelInfo
	LogLevelWarn
	LogLevelError
)

const (
	LogLevelDebugStr = "debug"
	LogLevelInfoStr  = "info"
	LogLevelWarnStr  = "warn"
	LogLevelErrorStr = "error"
)

var logLevelMap = map[string]LogLevel{
	LogLevelDebugStr: LegLevelDebug,
	LogLevelInfoStr:  LogLevelInfo,
	LogLevelWarnStr:  LogLevelWarn,
	LogLevelErrorStr: LogLevelError,
}

const (
	DefaultLogLevelStr = LogLevelInfoStr
	DefaultAdsbSbsHost = "localhost"
	DefaultAdsbSbsPort = 30003
)

type AppConfig struct {
	LogConfig      LogConfig
	AdsbConfig     AdsbConfig
	RecorderConfig RecorderConfig
}

type AdsbConfig struct {
	SbsConfig SbsConfig
}

type SbsConfig struct {
	Host string
	Port int
}

type RecorderConfig struct {
	Type    string
	Version uint
	Spec    map[string]interface{}
}

type SqlConfig struct {
}

type LogConfig struct {
	Level       LogLevel
	Development bool
}

func GetAppConfig() (cfg AppConfig, err error) {
	if cfg.LogConfig, err = GetLogConfig(); err != nil {
		return cfg, fmt.Errorf("Error getting log config: %w", err)
	}
	if cfg.AdsbConfig, err = GetAdsbConfig(); err != nil {
		return cfg, fmt.Errorf("Error getting adsb config: %w", err)
	}
	if cfg.RecorderConfig, err = GetRecorderConfig(); err != nil {
		return cfg, fmt.Errorf("Error getting recorder config: %w", err)
	}
	return
}

func GetLogConfig() (cfg LogConfig, err error) {
	// set development either from log.development or from log.dev
	if viper.IsSet("log.development") {
		cfg.Development = viper.GetBool("log.development")
	} else if viper.IsSet("log.dev") {
		cfg.Development = viper.GetBool("log.dev")
	}
	levelStr := viper.GetString("log.level")
	if levelStr == "" {
		cfg.Level = logLevelMap[DefaultLogLevelStr]
	} else if level, ok := logLevelMap[strings.ToLower(levelStr)]; ok {
		cfg.Level = level
	} else {
		return cfg, fmt.Errorf("Invalid log level: %s", levelStr)
	}
	return cfg, nil
}

func getZapLevel(level LogLevel) zapcore.Level {
	switch level {
	case LegLevelDebug:
		return zap.DebugLevel
	case LogLevelInfo:
		return zap.InfoLevel
	case LogLevelWarn:
		return zap.WarnLevel
	case LogLevelError:
		return zap.ErrorLevel
	default:
		return getZapLevel(logLevelMap[DefaultLogLevelStr])
	}
}

func (cfg LogConfig) GetZapLevel() zapcore.Level {
	return getZapLevel(cfg.Level)
}

func GetAdsbConfig() (cfg AdsbConfig, err error) {
	if cfg.SbsConfig, err = GetSbsConfig(); err != nil {
		return cfg, fmt.Errorf("Error getting sbs config: %w", err)
	}
	return cfg, nil
}

func GetSbsConfig() (cfg SbsConfig, err error) {
	if cfg.Host = viper.GetString("adsb.sbs.host"); cfg.Host == "" {
		cfg.Host = DefaultAdsbSbsHost
	}
	if cfg.Port = viper.GetInt("adsb.sbs.port"); cfg.Port == 0 {
		cfg.Port = DefaultAdsbSbsPort
	}
	return cfg, nil
}

func GetRecorderConfig() (cfg RecorderConfig, err error) {
	cfg.Type = viper.GetString("recorder.type")
	cfg.Version = viper.GetUint("recorder.version")
	cfg.Spec = viper.GetStringMap("recorder.spec")
	return cfg, nil
}

func (cfg RecorderConfig) GetRecorder() (recorder.Recorder, error) {
	return recorder.GetConfiguredRecorder(cfg.Type, cfg.Version, cfg.Spec)
}

func AddAppPersistentFlags(cmd *cobra.Command, bind bool) {
	// log flags
	cmd.PersistentFlags().String("log.level", DefaultLogLevelStr, "log level")
	rootCmd.PersistentFlags().String("adsb.sbs.host", DefaultAdsbSbsHost, "sbs server host")
	rootCmd.PersistentFlags().Int("adsb.sbs.port", DefaultAdsbSbsPort, "sbs server port")

	if bind {
		if err := viper.BindPFlags(cmd.PersistentFlags()); err != nil {
			panic(any(err))
		}
	}
}
