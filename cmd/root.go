/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"gitlab.com/chef1991/adsb/internal/log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

var rootConfig AppConfig

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "adsb",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		fmt.Println("rootCmd::PersistentPreRunE")
		if rootConfig, err = GetAppConfig(); err != nil {
			return fmt.Errorf("Error getting configuring application: %w", err)
		}
		//zapLogLevel := zap.NewAtomicLevelAt(rootConfig.LogConfig.GetZapLevel())
		//zapCfg := zap.Config{
		//	Level:       zapLogLevel,
		//	Development: rootConfig.LogConfig.Development,
		//}
		//if tmpLogger, err := zapCfg.Build(); err != nil {
		//	panic(fmt.Errorf("error building logger: %w", err))
		//} else {
		//	log.SetGlobalLogger(tmpLogger)
		//}
		if tmpLogger, err := zap.NewDevelopment(); err != nil {
			panic(fmt.Errorf("error building logger: %w", err))
		} else {
			log.SetGlobalLogger(tmpLogger)
		}
		logger := log.Logger
		sLogger := logger.Sugar()

		sLogger.Infow("rootCmd::PersistentPostRunE", "config", rootConfig)
		return nil
	},
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.adsb.yaml)")
	AddAppPersistentFlags(rootCmd, true)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".adsb" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".adsb")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
