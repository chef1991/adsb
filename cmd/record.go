/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"bufio"
	"fmt"
	"github.com/skypies/adsb"
	"github.com/spf13/cobra"
	"gitlab.com/chef1991/adsb/internal/log"
	"net"
)

// recordCmd represents the record command
var recordCmd = &cobra.Command{
	Use:   "record",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := log.Logger
		sLogger := logger.Sugar()
		logger.Info("recordCmd::Run")
		limitRun := 10000
		runCount := 0

		recorder, err := rootConfig.RecorderConfig.GetRecorder()
		if err != nil {
			panic(fmt.Errorf("error getting recorder: %w", err))
		}
		if err := recorder.Open(); err != nil {
			panic(fmt.Errorf("error opening recorder: %w", err))
		}
		defer recorder.Close()

		sbsAddress := fmt.Sprintf("%s:%d", rootConfig.AdsbConfig.SbsConfig.Host, rootConfig.AdsbConfig.SbsConfig.Port)
		sLogger.Debugw("connecting to sbs host", "address", sbsAddress)
		conn, err := net.Dial("tcp", sbsAddress)
		if err != nil {
			sLogger.Fatalw("error connecting to sbs host", "error", err)
		}
		defer func() {
			sLogger.Debugw("closing connection to sbs host", "address", sbsAddress)
			if err := conn.Close(); err != nil {
				sLogger.Warnw("error closing connection to sbs host", "error", err)
			} else {
				sLogger.Debugw("connection to sbs host closed", "address", sbsAddress)
			}
		}()
		sLogger.Debugw("connected to sbs host", "address", sbsAddress)

		connbuf := bufio.NewReader(conn)
		for {
			if limitRun > 0 && runCount >= limitRun {
				break
			}
			runCount++
			msgStr, err := connbuf.ReadString('\n')
			if err != nil {
				sLogger.Errorw("error reading from sbs host", "error", err)
				break
			}
			sLogger.Debugw("read from sbs host", "message", msgStr, "messageLength", len(msgStr))
			if len(msgStr) > 0 {
				var msg adsb.Msg
				if err := msg.FromSBS1(msgStr); err != nil {
					sLogger.Errorw("error parsing sbs message", "error", err)
				} else {
					sLogger.Debugw("parsed sbs message", "message", msg)
					sLogger.Debugw("recording message", "message", msg)
					if err := recorder.RecordMessage(&msg); err != nil {
						sLogger.Warnw("error recording message", "error", err)
					} else {
						sLogger.Debugw("message recorded", "message", msg)
					}
				}

			}
		}
	},
}

func init() {
	rootCmd.AddCommand(recordCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// recordCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// recordCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
